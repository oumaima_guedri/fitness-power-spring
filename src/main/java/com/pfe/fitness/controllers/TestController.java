package com.pfe.fitness.controllers;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api/test")
public class TestController {
	
	
	@GetMapping("/all")
	public String allAccess() {
		return "Public Content.";
	}
	
	@GetMapping("/abonne")
	@PreAuthorize("hasRole('ABONNE') or hasRole('ENTRAINEUR') or hasRole('ADMIN')")
	public String abonneAccess() {
		return "Abonne Content.";
	}

	@GetMapping("/entraineur")
	@PreAuthorize("hasRole('ENTRAINEUR')")
	public String entraineurAccess() {
		return "Entraineur Board.";
	}

	@GetMapping("/admin")
	@PreAuthorize("hasRole('ADMIN')")
	public String adminAccess() {
		return "Admin Board.";
	}
}
