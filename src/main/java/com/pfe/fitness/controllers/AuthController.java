package com.pfe.fitness.controllers;

import java.util.*;
import java.util.stream.Collectors;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;

import com.pfe.fitness.entities.ERole;
import com.pfe.fitness.entities.Role;
import com.pfe.fitness.entities.User;
import com.pfe.fitness.payload.request.LoginRequest;
import com.pfe.fitness.payload.request.RegisterRequest;
import com.pfe.fitness.payload.response.JwtResponse;
import com.pfe.fitness.payload.response.MessageResponse;
import com.pfe.fitness.repository.RoleRepository;
import com.pfe.fitness.repository.UserRepository;
import com.pfe.fitness.security.jwt.JwtUtils;
import com.pfe.fitness.security.services.UserDetailsImpl;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api/auth")
public class AuthController {
	@Autowired
	AuthenticationManager authenticationManager;

	@Autowired
	UserRepository userRepository;

	@Autowired
	RoleRepository roleRepository;

	@Autowired
	PasswordEncoder encoder;

	@Autowired
	JwtUtils jwtUtils;

	@PostMapping("/login")
	public ResponseEntity<?> authenticateUser(@Valid @RequestBody LoginRequest loginRequest) {
		
	Authentication authentication = authenticationManager.authenticate(
		new UsernamePasswordAuthenticationToken(loginRequest.getEmail(), loginRequest.getPassword()));

		SecurityContextHolder.getContext().setAuthentication(authentication);
		String jwt = jwtUtils.generateJwtToken(authentication);
		
		UserDetailsImpl userDetails = (UserDetailsImpl) authentication.getPrincipal();		
		List<String> roles = userDetails.getAuthorities().stream()
				.map(item -> item.getAuthority())
				.collect(Collectors.toList());

		return ResponseEntity.ok(new JwtResponse(jwt, 
												 userDetails.getId(), 
												 userDetails.getNom(),
												 userDetails.getPrenom(), 
												 userDetails.getDatenaiss(), 
												 userDetails.getEmail(), 
												 userDetails.getTelephone(), 
												 userDetails.getTypesport(),
												 userDetails.getPoids(),
												 roles));
	}

	@PostMapping("/register")
	public ResponseEntity<?> registerUser(@Valid @RequestBody RegisterRequest registerRequest) {
		if (userRepository.existsByprenom(registerRequest.getPrenom())) {
			return ResponseEntity
					.badRequest()
					.body(new MessageResponse("Error: Username is already taken!"));
		}

		if (userRepository.existsByEmail(registerRequest.getEmail())) {
			return ResponseEntity
					.badRequest()
					.body(new MessageResponse("Error: Email is already in use!"));
		}

		// Create new user's account
		User user = new User(registerRequest.getNom(),registerRequest.getPrenom(),registerRequest.getDatenaiss(),registerRequest.getEmail(),encoder.encode(registerRequest.getPassword()),
				registerRequest.getTelephone(),registerRequest.getTypesport(),registerRequest.getPoids());
				
		Set<String> strRoles = registerRequest.getRole();
		Set<Role> roles = new HashSet<>();
		
		if (strRoles == null) {
			Role abonneRole = roleRepository.findByName(ERole.ABONNE)
					.orElseThrow(() -> new RuntimeException("Error: Role is not found."));
			roles.add(abonneRole);
		} else {
			strRoles.forEach(role -> {
				switch (role) {
				case "admin":
					Role adminRole = roleRepository.findByName(ERole.ADMIN)
							.orElseThrow(() -> new RuntimeException("Error: Role is not found."));
					roles.add(adminRole);
					
					break;
				case "entraineur":
					Role entraineurRole = roleRepository.findByName(ERole.ENTRAINEUR)
							.orElseThrow(() -> new RuntimeException("Error: Role is not found."));
					roles.add(entraineurRole);

					break;
				default:
					Role abonneRole = roleRepository.findByName(ERole.ABONNE)
							.orElseThrow(() -> new RuntimeException("Error: Role is not found."));
					roles.add(abonneRole);
				}
			}); 
		}
        user.setRoles(roles);
		userRepository.save(user);
		return ResponseEntity.ok(new MessageResponse("User registered successfully!"));
	}
}
