package com.pfe.fitness.services;

import java.util.List;
import org.springframework.security.core.userdetails.UserDetailsService;

import com.pfe.fitness.entities.User;

public interface UtilisateurServices extends UserDetailsService {
	
	//CRUD 
	public List<User> getAllUtilisateurs();
	public User findUtilisateurById(Long id);
	public User updateUtlisateur(User utilisateur);
	public void deleteUtlisateur(Long id);
	
	
	//Other 
	//public List<Utilisateur> findByFirstName(String firstname);
	//public List<Utilisateur> findByFirstNameAndLastName(String firstname, String lastname);
	//public List<Utilisateur> findByRolesName(ERole name);
	
	
	

}

